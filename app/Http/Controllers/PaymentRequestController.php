<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PaymentRequest;
use App\Http\Traits\FileUpload;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\PaymentFormRequest;

class PaymentRequestController extends Controller
{
    use FileUpload;
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Session()->put('data_url',request()->fullUrl());

        if (admin()) {
            $payments = PaymentRequest::AdminView();
            return view('payments.index', compact('payments'));
        }

        $payments = PaymentRequest::UserView();

        return view('payments.index', compact('payments'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // abort_if(Gate::denies('create-request'), 403, 'Only users can create requests.');
        abort_if(Auth::user()->cannot('create-request'), 403, 'Only users can create requests.');
        
        return view('payments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PaymentFormRequest $request)
    {
        $paymentRequest = new PaymentRequest;
        $paymentRequest->storePayment($request);

        session()->flash('status', 'Successfully created!');
        session()->flash('description', 'Waiting to be approved or denied.');

        return redirect()->route('payments.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PaymentRequest  $paymentRequest
     * @return \Illuminate\Http\Response
     */
    public function show(PaymentRequest $paymentRequest)
    {
        // abort_if(Gate::denies('show-request', $paymentRequest), 403, 'You do not have the permission to see this page.');
        abort_if(Auth::user()->cannot('show-request', $paymentRequest), 403, 'You do not have the permission to see this page.');

        return view('payments.show', compact('paymentRequest'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PaymentRequest  $paymentRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(PaymentRequest $paymentRequest)
    {
        
        // abort_if(Gate::denies('edit-request', $paymentRequest), 403, 'Editing request is only allowed for users and while it hasn\'t been approved or denied.');
        abort_if(Auth::user()->cannot('edit-request', $paymentRequest), 403, 'Editing request is only allowed for users and while it hasn\'t been approved or denied.');

        return view('payments.edit', compact('paymentRequest'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PaymentRequest  $paymentRequest
     * @return \Illuminate\Http\Response
     */
    public function update(PaymentFormRequest $request, PaymentRequest $paymentRequest)
    {
        $paymentRequest->updatePayment($request);

        session()->flash('status', 'Successfully edited!');
        session()->flash('description', 'Waiting to be approved or denied.');

        if (Session('data_url')) {
            return redirect(Session('data_url'));
        }

        return redirect()->route('payments.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PaymentRequest  $paymentRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(PaymentRequest $paymentRequest)
    {
        // abort_if(Gate::denies('delete-request', $paymentRequest), 403, 'Only users can delete a request while it hasn\'t been approved or denied.');
        abort_if(Auth::user()->cannot('delete-request', $paymentRequest), 403, 'Only users can delete a request while it hasn\'t been approved or denied.');
        
        $paymentRequest->delete();

        session()->flash('status', 'Successfully deleted!');

        return redirect()->route('payments.index');
    }

    public function approved(PaymentRequest $paymentRequest)
    {
        // abort_if(Gate::denies('approve-request'), 403, 'Only admins can approve a request.');
        abort_if(Auth::user()->cannot('approve-request'), 403, 'Only admins can approve a request.');
        
        $paymentRequest->approve();

        if (Session('data_url')) {
            return redirect(Session('data_url'));
        }

        return redirect()->route('payments.index');
    }

    public function denied(Request $request, PaymentRequest $paymentRequest)
    {
        // abort_if(Gate::denies('deny-request'), 403, 'Only admins can deny a request.');
        abort_if(Auth::user()->cannot('deny-request'), 403, 'Only admins can deny a request.');
        
        $request->validate([
            'rejection'=> 'required'
        ]);

        $paymentRequest->deny($request);

        return redirect()->route('payments.index');
    }
}
