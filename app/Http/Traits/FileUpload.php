<?php

namespace App\Http\Traits;

use App\Http\Requests\PaymentFormRequest;

trait FileUpload
{
    public function fileUpload(PaymentFormRequest $request)
    {        
        
        $fileName = time().'_'.$request->file->getClientOriginalName();
        $filePath = $request->file('file')->storeAs('uploads', $fileName, 'public');

        return $filePath;
    }
}
