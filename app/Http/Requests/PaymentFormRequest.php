<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaymentFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'title'=> 'required|min:3|max:200',
            'price'=> 'required|integer',
            'description'=> 'required',
            'file' => 'required|mimes:png,jpg,jpeg|dimensions:min_width=100,min_height=200'
        ];
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'price' => to_english_numbers($this->input('price'))
        ]);
    }
}
