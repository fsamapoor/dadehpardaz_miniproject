<?php

namespace App\Providers;

use App\Models\PaymentRequest;
use App\Models\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //Show payment requests only to the request owner OR admin.
        Gate::define('show-request', function(User $user, PaymentRequest $paymentRequest){
            return ( $user->isAdmin() || $user->id === $paymentRequest->user_id );
        });
        
        // Editing request is only allowed for users and while it hasn't been approved or denied.
        Gate::define('edit-request', function(User $user, PaymentRequest $paymentRequest){
            return ( $paymentRequest->isWaiting() && !$user->isAdmin() );
        });
        
        // Only admins can approve or deny a request.
        Gate::define('approve-request', function(User $user){
            return ( $user->isAdmin() );
        });
        
        // Only admins can approve or deny a request.
        Gate::define('deny-request', function(User $user){
            return ( $user->isAdmin() );
        });

        // Only users can create a request.
        Gate::define('create-request', function(User $user){
            return ( !$user->isAdmin() );
        });

        // Only users can delete a request while it hasn't been approved or denied.
        Gate::define('delete-request', function(User $user, PaymentRequest $paymentRequest){
            return ( $paymentRequest->isWaiting() && !$user->isAdmin() );
        });
    }
}
