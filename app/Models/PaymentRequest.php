<?php

namespace App\Models;

use App\Models\User;
use App\Http\Traits\FileUpload;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use App\Http\Requests\PaymentFormRequest;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PaymentRequest extends Model
{
    use HasFactory, FileUpload;

    protected $guarded = [];

    /**
     * Get the user that owns the payment request.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    
    const PAYMENT_STATUS_WAITING = 1;
    const PAYMENT_STATUS_APPROVED = 2;
    const PAYMENT_STATUS_REJECTED = 3;
    const PAYMENT_STATUS_INPROGRESS = 4;

    
    public function isWaiting() {
        return $this->status === self::PAYMENT_STATUS_WAITING;
    }
    
    public function isApproved() {
        return $this->status === self::PAYMENT_STATUS_APPROVED;
    }
    
    public function isRejected() {
        return $this->status === self::PAYMENT_STATUS_REJECTED;
    }
    
    public function isProgress() {
        return $this->status === self::PAYMENT_STATUS_INPROGRESS;
    }

    public function scopeUserView($query)
    {
        return $query->where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->paginate(4);
    }
    
    public function scopeAdminView($query)
    {
        return $query->with('user:id,name')->latest()->paginate(4);
    }

    public function storePayment(PaymentFormRequest $request)
    {
        // $data =  $request->input();

        // foreach ($data as $key => $value) {
        //     $this->$key = $request->$key;
        // }
        
        $this->title = $request->title;
        $this->price = $request->price;
        $this->description = $request->description;
        $this->user_id = Auth::user()->id;
        $this->file_path = $this->fileUpload($request);
        $this->code = time().'-'. Auth::user()->id .'-'.rand(1000001,9999999);

        $this->save();
    }

    public function updatePayment(PaymentFormRequest $request)
    {
        $this->title = $request->title;
        $this->price = $request->price;
        $this->description = $request->description;
        $this->file_path = $this->fileUpload($request);

        $this->save();
    }

    public function approve()
    {
        $this->status = self::PAYMENT_STATUS_APPROVED;
        $this->admin_note = null;
        $this->added_at = now();

        $this->save();
    }

    public function deny($request)
    {
        $this->status = self::PAYMENT_STATUS_REJECTED;
        $this->admin_note = $request->rejection;
        $this->added_at = now();

        $this->save();
    }
}