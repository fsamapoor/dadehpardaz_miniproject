<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PaymentRequestController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::middleware(['auth'])->prefix('/payment-requests')->name('payments.')->group(function () {

    Route::get('/', [PaymentRequestController::class, 'index'])->name('index');
    Route::get('/create', [PaymentRequestController::class, 'create'])->name('create');
    Route::get('/{paymentRequest}', [PaymentRequestController::class, 'show'])->name('show');
    Route::post('/', [PaymentRequestController::class, 'store'])->name('store');
    Route::get('/{paymentRequest}/edit', [PaymentRequestController::class, 'edit'])->name('edit');
    Route::put('/{paymentRequest}', [PaymentRequestController::class, 'update'])->name('update');
    Route::delete('/{paymentRequest}', [PaymentRequestController::class, 'destroy'])->name('destroy');
    Route::post('/{paymentRequest}/approved', [PaymentRequestController::class, 'approved'])->name('approved');
    Route::post('/{paymentRequest}/denied', [PaymentRequestController::class, 'denied'])->name('denied');
});




require __DIR__.'/auth.php';


