<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Payment Requests') }}
            </h2>
            @if (!admin())
                <a class="no-underline btn bg-indigo-600 hover:bg-indigo-700 focus:ring-indigo-500" href="{{ route('payments.create') }}">{{ __('Create') }}</a>
            @endif
        </div>
    </x-slot>

    @if (session('status'))
        <!-- This example requires Tailwind CSS v2.0+ -->
        <!-- Global notification live region, render this permanently at the end of the document -->
        <div id="notification" aria-live="assertive" class="transition-opacity opacity-100 fixed inset-0 flex items-end px-4 py-6 pointer-events-none sm:p-6">
            <div class="w-full flex flex-col items-center space-y-4 sm:items-end">
            <!--
                Notification panel, dynamically insert this into the live region when it needs to be displayed

                Entering: "transform ease-out duration-300 transition"
                From: "translate-y-2 opacity-0 sm:translate-y-0 sm:translate-x-2"
                To: "translate-y-0 opacity-100 sm:translate-x-0"
                Leaving: "transition ease-in duration-100"
                From: "opacity-100"
                To: "opacity-0"
            -->
            <div class="max-w-sm w-full bg-green-50 shadow-lg rounded-lg pointer-events-auto ring-1 ring-black ring-opacity-5 overflow-hidden">
                <div class="p-4">
                <div class="flex items-start">
                    <div class="flex-shrink-0">
                    <!-- Heroicon name: outline/check-circle -->
                    <svg class="h-6 w-6 text-green-400" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z" />
                    </svg>
                    </div>
                    <div class="@if(is_locale_fa()) mr-3 @else ml-3 @endif w-0 flex-1 pt-0.5">
                        <p class="text-sm font-medium text-gray-900">
                            <!-- {{ __('Successfully created!') }} -->
                            {{ __(session('status')) }}
                        </p>
                    @if (session()->has('description'))
                        <p class="mt-1 text-sm text-gray-500">
                            {{ __(session('description')) }}
                        </p>
                    @endif
                    </div>
                    <div class="@if(is_locale_fa()) mr-4 @else ml-4 @endif flex-shrink-0 flex">
                    <button onclick="hideNotification()" class="bg-white rounded-md inline-flex text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        <span class="sr-only">{{ __('Close') }}</span>
                        <!-- Heroicon name: solid/x -->
                        <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                        <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd" />
                        </svg>
                    </button>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
    @endif

    <div class="mt-1 flex flex-col max-w-5xl mx-auto">
        <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
            <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
            <table class="min-w-full divide-y divide-gray-200">
                <thead class="bg-gray-50">
                <tr>
                    @if (admin())
                        <th scope="col" class="table-col-header @if ('fa-IR' == app()->getLocale()) text-right @else text-left @endif">
                            {{ __('Name') }}
                        </th>
                    @endif
                    <th scope="col" class="table-col-header @if ('fa-IR' == app()->getLocale()) text-right @else text-left @endif">
                        {{ __('Title') }}
                    </th>
                    <th scope="col" class="table-col-header @if ('fa-IR' == app()->getLocale()) text-right @else text-left @endif">
                        {{ __('price') }}
                    </th>
                    <th scope="col" class="table-col-header @if ('fa-IR' == app()->getLocale()) text-right @else text-left @endif">
                        {{ __('Status') }}
                    </th>
                    <th scope="col" class="table-col-header @if ('fa-IR' == app()->getLocale()) text-right @else text-left @endif">
                        {{ __('Last Updated') }}
                    </th>
                    @if (admin())
                        <th scope="col" class="table-col-header @if ('fa-IR' == app()->getLocale()) text-right @else text-left @endif">
                           {{ __('Action') }}
                        </th>
                    @endif
                    <th scope="col" class="relative px-6 py-3">
                    <span class="sr-only">{{ __('Show') }}</span>
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach ($payments as $payment)
                <tr class="@if ($loop->even) bg-white @else bg-gray-100 @endif">
                    @if (admin())
                        <td class="table-data font-medium text-gray-900">
                        {{ $payment->user->name }}
                        </td>
                    @endif
                    <td class="table-data text-gray-500">
                        {{ $payment->title }}
                    </td>
                    <td class="table-data text-gray-500">
                        {{ $payment->price }}
                    </td>
                    <td class="table-data text-gray-500">
                    @if ($payment->isWaiting())
                    <span class="status bg-blue-500">
                    {{ __('Waiting') }}
                    </span>
                    @elseif ($payment->isApproved())
                    <span class="status bg-green-600">
                    {{ __('Approved') }}
                    </span>
                    @else
                    <span class="status bg-red-500">
                    {{ __('Rejected') }}
                    </span>
                    @endif
                    </td>
                    <td class="table-data text-gray-500 font-medium">
                        {{ $payment->updated_at }}
                    </td>
                    <td class="table-data font-medium">
                    @if (!$payment->isApproved() && admin())
                        <form action="{{ route('payments.approved', ['paymentRequest' => $payment->id]) }}" method="POST">
                            @csrf
                            <button type="submit" class="btn-md bg-green-700 hover:bg-green-800 focus:ring-green-500">{{ __('Approve') }}</button>
                        </form>
                    @endif
                    </td>
                    <td class="table-data text-right font-medium">
                        <a href="{{ route('payments.show', ['paymentRequest' => $payment->id]) }}" class="text-indigo-600 hover:text-indigo-900">
                            {{ __('Show') }}
                        </a>
                    </td>
                </tr>
                @endforeach

                </tbody>
            </table>
            </div>
        </div>
        </div>
    </div>

    <div class="max-w-5xl mx-auto">
        {{ $payments->links('vendor.pagination.tailwind') }}
    </div>

    <script>
        function hideNotification(){
            document.querySelector("#notification").style.display = "none";
        }
    </script>


</x-app-layout>
