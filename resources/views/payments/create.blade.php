<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Requests') }}
        </h2>
    </x-slot>


    <div class="mt-1 md:grid md:grid-cols-4 md:gap-6 max-w-5xl mx-auto">
        {{-- <div class="mt-5 md:mt-0 md:col-span-1">
            <div class="px-4 sm:px-0">
                <h3 class="text-lg font-medium leading-6 text-gray-900">New Payment Request</h3>
                <p class="mt-1 text-sm text-gray-600">
                This information will be displayed publicly so be careful what you share.
                </p>
            </div>
        </div> --}}

        <div class="mt-5 md:mt-0 md:col-span-4">
            <form action="{{ route('payments.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="shadow sm:rounded-md sm:overflow-hidden">
                    <div class="px-4 py-5 bg-white space-y-6 sm:p-6">

                        <div class="grid grid-cols-6 gap-6">
                            <div class="col-span-6 sm:col-span-3">
                                <label for="title" class="input-label">{{ __('Title') }}</label>
                                <input type="text" id="title" name="title" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md @error('title') form-control-invalid @enderror" value="{{ old('title') }}" placeholder="{{ __('Title') }}">
                                @error('title')
                                    <p class="block text-red-700">
                                        {{ $message }}
                                    </p>
                                @enderror
                            </div>

                            <div class="col-span-6 sm:col-span-3">
                                <label for="price" class="input-label">{{ __('Price') }}</label>
                                <div class="relative">
                                    @if (!is_locale_fa())
                                        <div class="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                                            <span class="text-gray-500 sm:text-sm">
                                                $
                                            </span>
                                        </div>
                                    @endif
                                    <input @if(is_locale_fa()) dir="ltr" @endif type="text" id="price" name="price" class="@if(is_locale_fa()) pl-14 @else pl-7 @endif mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md @error('price') form-control-invalid @enderror" value="{{ old('price') }}">
                                    <div class="absolute top-2 @if(is_locale_fa()) left-0 pl-3 @else right-0 pr-3 @endif flex items-center pointer-events-none">
                                       <span class="text-gray-500 sm:text-sm" id="price-currency">
                                        @if(is_locale_fa())
                                            تومان
                                        @else     
                                            USD
                                        @endif
                                        </span>
                                    </div> 
                                    @error('price')
                                        <p class="block text-red-700">
                                            {{ $message }}
                                        </p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="mt-3">
                            <label for="description" class="input-label">{{ __('Description') }}</label>
                            <div class="mt-1">
                                <textarea type="text" id="description" name="description" class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 mt-1 block w-full sm:text-sm border border-gray-300 rounded-md @error('description') form-control-invalid @enderror"  placeholder="{{ __('Description') }}">{{ old('description') }}</textarea>
                            </div>
                            <p class="mt-2 text-sm text-gray-500">
                                {{ __('Brief description for your payment request.') }}
                            </p>
                            @error('description')
                                <p class="block text-red-700">
                                    {{ $message }}
                                </p>
                            @enderror
                        </div>

                        <div>
                            <label class="input-label">
                            {{ __('Document') }}
                            </label>
                            <div class="mt-1 flex justify-center px-6 pt-5 pb-6 border-2 border-gray-300 border-dashed rounded-md">
                            <div class="space-y-1 text-center">
                                <svg class="mx-auto h-12 w-12 text-gray-400" stroke="currentColor" fill="none" viewBox="0 0 48 48" aria-hidden="true">
                                <path d="M28 8H12a4 4 0 00-4 4v20m32-12v8m0 0v8a4 4 0 01-4 4H12a4 4 0 01-4-4v-4m32-4l-3.172-3.172a4 4 0 00-5.656 0L28 28M8 32l9.172-9.172a4 4 0 015.656 0L28 28m0 0l4 4m4-24h8m-4-4v8m-12 4h.02" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" />
                                </svg>
                                <div class="flex text-sm text-gray-600">
                                <label for="file" class="relative cursor-pointer bg-white rounded-md font-medium text-indigo-600 hover:text-indigo-500 focus-within:outline-none focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-indigo-500">
                                    <span class="mx-auto">Upload a file</span>
                                    <input id="file" name="file" type="file" class="sr-only">
                                </label>
                                {{-- <p class="pl-1">or drag and drop</p> --}}
                                </div>
                                <p class="text-xs text-gray-500">
                                PNG, JPG
                                </p>
                            </div>
                            </div>
                            @error('file')
                            <p class="block text-red-700">
                                {{ $message }}
                            </p>
                            @enderror
                        </div>
                    </div>

                    <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                        <a href="{{ route('payments.index') }}" class="@if(is_locale_fa()) ml-1 @else mr-1 @endif no-underline btn bg-black hover:bg-gray-700 focus:ring-gray-500">
                            {{ __('Back to the list') }}
                        </a>
                        <button type="submit" class="btn bg-indigo-600 hover:bg-indigo-700 focus:ring-indigo-500">
                        {{ __('Create') }}
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>


</x-app-layout>
