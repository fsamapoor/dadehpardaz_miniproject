<x-app-layout>
    <x-slot name="header">
        <div class="flex justify-between align-items-center">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                {{ __('Show Requests') }}
            </h2>
            <div class="justify-end">
                @if ($paymentRequest->added_at)
                <span class="no-underline btn bg-green-600 hover:bg-green-700 focus:ring-indigo-500" href="#">{{ __('Evaluated at:') }} {{ $paymentRequest->added_at }}</span>
                @endif
                <span class="no-underline btn bg-indigo-600 hover:bg-indigo-700 focus:ring-indigo-500" href="{{ route('payments.create') }}">{{ __('Code:') }} {{ $paymentRequest->code }}</span>
            </div>
        </div>
    </x-slot>


    <div class="mt-1 md:grid md:grid-cols-4 md:gap-6 max-w-5xl mx-auto">
        {{-- <div class="mt-5 md:mt-0 md:col-span-1">
            <div class="px-4 sm:px-0">
                <h3 class="text-lg font-medium leading-6 text-gray-900">New Payment Request</h3>
                <p class="mt-1 text-sm text-gray-600">
                This information will be displayed publicly so be careful what you share.
                </p>
            </div>
        </div> --}}

        <div class="mt-5 md:mt-0 md:col-span-4">
            <form action="{{ route('payments.denied', ['paymentRequest' => $paymentRequest->id]) }}" method="POST">
                <div class="shadow sm:rounded-md sm:overflow-hidden">
                    <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                        <div class="grid grid-cols-6 gap-6">
                            <div class="col-span-6 sm:col-span-3">
                                <label for="title" class="input-label">{{ __('Title') }}</label>
                                <div id="title" type=text class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                    {{ $paymentRequest->title }}
                                </div>
                            </div>

                            <div class="col-span-6 sm:col-span-3">
                                <label for="price" class="input-label">{{ __('Price') }}</label>
                                <div class="relative mt-1">
                                @if (!is_locale_fa())
                                    <div class="absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none">
                                        <span class="text-gray-500 sm:text-sm">
                                            $
                                        </span>
                                    </div>
                                @endif
                                <div @if(is_locale_fa()) dir="ltr" @endif type="text" id="price" class="@if(is_locale_fa()) pl-14 @else pl-7 @endif mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                    {{ $paymentRequest->price }}
                                </div>
                                <div class="absolute inset-y-0 @if(is_locale_fa()) left-0 pl-3 @else right-0 pr-3 @endif flex items-center pointer-events-none">
                                    <span class="text-gray-500 sm:text-sm" id="price-currency">
                                    @if(is_locale_fa())
                                        تومان
                                    @else     
                                        USD
                                    @endif
                                    </span>
                                </div> 
                            </div>
                            </div>
                        </div>

                        <div class="mt-3">
                            <label for="description" class="input-label">{{ __('Description') }}</label>
                            <div class="mt-1">
                                <div type="text" id="description" class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 mt-1 block w-full sm:text-sm border border-gray-300 rounded-md">{{ $paymentRequest->description }}
                                </div>
                            </div>
                        </div>

                        <div>
                            <label class="input-label">
                            {{ __('Document') }}
                            </label>
                            <div class="mt-1 flex justify-center px-6 pt-5 pb-6 border-2 border-gray-300 border-dashed rounded-md">
                                <img src="{{ asset('storage').'/'.$paymentRequest->file_path }}">
                            </div>
                        </div>

                        @if ($paymentRequest->admin_note && Auth::user()->isUser())
                            <div>
                                <label for="Rejection" class="input-label">{{ __('Rejection Cause') }}</label>
                                <div type="text" id="Rejection" name="rejection" class="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border border-gray-300 rounded-md">
                                    {{ $paymentRequest->admin_note }}
                                </div>
                            </div>
                        @endif

                        @if (admin())
                        <div>
                            <label for="Rejection" class="input-label">{{ __('Rejection Cause') }}</label>
                            <input type="text" id="Rejection" name="rejection" class="mt-1 shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border border-gray-300 rounded-md @error('rejection') form-control-invalid @enderror" value="{{ $paymentRequest->admin_note }}">
                            @error('rejection')
                            <p class="text-red-700">{{ $message }}</p>
                            @enderror
                            @csrf
                            <button type="submit" class="mt-2 btn bg-red-700 hover:bg-red-800 focus:ring-red-500">{{ __('Reject') }}</button>
                        </div>
                        @endif
                    </div>
                </div>
            </form>

            <div class="flex justify-end px-4 py-3 bg-gray-50 text-right sm:px-6">
                <a href="{{ route('payments.index') }}" class="@if(is_locale_fa()) ml-2 @else mr-2 @endif no-underline btn bg-black hover:bg-gray-700 focus:ring-gray-500">
                    {{ __('Back to the list') }}
                </a>

                @if (Auth::user()->isUser() && $paymentRequest->isWaiting())
                    <a href="{{ route('payments.edit', ['paymentRequest' => $paymentRequest->id]) }}" class="@if(is_locale_fa()) ml-2 @else mr-2 @endif no-underline btn bg-blue-700 hover:bg-blue-800 focus:ring-blue-500">
                    {{ __('Edit') }}
                    </a>

                    <form action="{{ route('payments.destroy', ['paymentRequest' => $paymentRequest->id]) }}" method="POST">
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn bg-red-700 hover:bg-red-800 focus:ring-red-500">{{ __('Delete') }}</button>
                    </form>
                @endif

                @if (admin())
                    <form action="{{ route('payments.approved', ['paymentRequest' => $paymentRequest->id]) }}" method="POST">
                        @csrf
                        <button type="submit" class="btn bg-green-700 hover:bg-green-800 focus:ring-green-500">{{ __('Approve') }}</button>
                    </form>
                @endif
            </div>
        </div>
    </div>


</x-app-layout>
