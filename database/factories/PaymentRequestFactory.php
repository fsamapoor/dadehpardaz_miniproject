<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class PaymentRequestFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $faker = \Faker\Factory::create();

        return [
            'title' => $faker->firstName(),
            'price' => random_int(1,1000),
            'user_id' => 2,
            'description' => $faker->realText(rand(10,200)),
            'code' => time().'-'.rand(1000001,9999999)
        ];
    }
}
